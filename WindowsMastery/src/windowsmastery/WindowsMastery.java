/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowsmastery;
import java.util.*;

/**
 *
 * @author apprentice
 */
public class WindowsMastery {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here        
               
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter height in feet: ");        
        double height = Double.parseDouble(sc.nextLine());
        
        System.out.println("Please enter width in feet: ");
        double width = Double.parseDouble(sc.nextLine());
        
        double area = height * width;
        double perimeter = height * 2 + width * 2;
                
        System.out.println("Please enter glass cost per sq. ft.: ");
        double glassCost = Double.parseDouble(sc.nextLine());
        
        System.out.println("Please enter trim cost per ft.: ");
        double trimCost = Double.parseDouble(sc.nextLine());
        
        double totalGlassCost = area * glassCost;
        double totalTrimCost = perimeter * trimCost;
        
        System.out.println("Height: " + height + " ft.");
        System.out.println("Width: " + width + " ft.");
        System.out.println("Area: " + area + " sq. ft.");
        System.out.println("Perimeter: " + perimeter + " ft.");
        System.out.println("Glass Cost : $" + 
                Math.floor(glassCost * 100) / 100 + " per sq. ft.");
        System.out.println("Trim Cost : $" + 
                Math.floor(trimCost * 100) / 100 + " per ft.");
        System.out.println();
        System.out.println("Material Cost: $" + 
                Math.floor(totalGlassCost * 100) / 100);
        System.out.println("Trim Cost: $" + 
                Math.floor(totalTrimCost * 100) / 100);
        System.out.println("TOTAL COST: $" + 
                Math.floor((totalGlassCost + totalTrimCost) * 100) / 100);
     
    }
    
}
